# BirdSoup

BirdSoup is a project that aims to visualize politician agreements on certain subjects. For a specific subject *A*, it can be described as the following pipeline :

* Collect politicians tweets on twitter (see _Twitter Scrapping_)
* Isolate tweets about subject *A*
* Compute agreements in time between politicians on subject *A*


# Quickstart

## Dependencies

The first step is to install the required python dependencies. This can be done inside a virtual env :

```sh
python -m venv env
source ./env/bin/activate
pip install -r requirements
```


## Scrapping from Twitter

Then, you must acquire datas from twitter. This can be done with the *scrap.py* script. The following command will download tweets from accounts listed in *figures.json*, from March 1st to today, and save the resulting datas in *twitter_scrap.json* :

```sh
python scrap.py --input-file figures.json --limit-date 2020-03-01 --output-file twitter_scrap.json
```


## Getting a NLI Model

To compute agreement in time between politician, you need a model capable of measuring agreement between two sentences. To that, we use a model trained on Natural Language Inference (NLI).

### Option 1 : Training a Model

You can train a SentenceBERT model on the MultiNLI dataset (we can't use the pretrained models given by the SentenceBERT authors, since the NLI classifier is not given). To train such a model :

```sh
python train_nli.py
```

The model is saved as *./models/default* by default.


### Option 2 : Downloading an Already Trained Model

If you don't want to train the model yourself (which is understandable, I spent 1h30 training it on my RTX 2060S, I don't think it's any fun really...), you can download the model I already trained using the *get_pretrained_nli.sh* script :

```sh
./get_pretrained_nli.sh
```

In order to perform this magic trick, you need *sh*, *wget* and *unzip*. Chances are, it works out of the box on Linux. If you're on windows and don't have those programs, you might want to reconsider your choices in life... or use this [manual download link](http://cloud.klmp200.net/index.php/s/6ZLis274T4HgbTz/download) to download the model, and unzip it under *./models/*.


## Exporting an Analyse

Lastly, use the *export_analyse.py* script to export an agreement-in-time analyse. Use the *--filter-pattern* to supply a regular expression (in the source language) to filter the tweets : any tweets not containing this expression will be discarded. This is used to select a specific subject. use `python export_analyse.py --help` for more details.


### English Inputs

If your input tweets are in english, you can use the script without worrying : 

```sh
python export_analyse.py --input-file twitter_scrap.json --filter-pattern '[Mm]asks?' --output-file analyse.json
```


### Other Languages

If your input language is not english, you can use the *--input-language* option to automatically translate (yes, using google translate) your input before agreement prediction. Use `python export_analyse.py --help` to see a list of available languages... or just use *'auto'* if you're unsure. Here is an example for french :

```sh
python export_analyse.py --input-file twitter_scrap.json --filter-pattern '[Mm]asques?' --input-language fr --output-file analyse.json
```


### Visualization

When your analyse is ready, you can use [DRay](https://gitlab.com/Aethor/dray) to visualize it as a graph !


# Twitter Scrapping

The _scrap.py_ script can be used to retrieve politician tweets. It uses *requests* and *BeautifulSoup*, and makes use of parallelism to gain speed.

It takes as input an file describing the accounts you want to parse (by default, it reads the *figures.json* file, which contains a list of accounts for french politicians), and a limit date. By default, it outputs the resulting datas in json format to standard output : redirect the stream or use the *--output-file* option to save them to a file. See `python scrap.py --help` for more details.


## Inner Working

To be able to scrap Twitter using BeautifulSoup, we use an older version of the website : the legacy one, still up and running for the mobile devices that can't handle javascript.
For that, we use a modified user agent to request the webpage, tricking the server into thinking our program is an old device.

We use the following user agent :

> Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)


## Example Politician Lists : French Politicians

The *figures.json* file contain an input file that can be used as input to *scrap.py*. It contains the following list of politicians. All followers number were extracted on 04/19/2020.

### La République en marche

- Emmanuel Macron : 5M followers, 
- Édouard Philippe : 646.4K followers, 
- Christophe Castaner : 295.1K followers


### Les Républicains

- Éric Ciotti : 155.9K followers
- Bruno Retailleau : 62.2k followers, 
- Éric Woerth : 58.9K followers


### Modem

- François Bayrou : 694.4K followers


### Socialistes

- Olivier Faure : 47.5k followers


### UDI

- Jean-Christophe Lagarde : 121.4K followers
- Sophie Auconie : 10.7k followers


### Liberté et Territoire

- Philippe Vigier : 11.5K followers


### La France Insoumise

- Jean-Luc Mélanchon : 2M followers
- François Ruffin : 171.3K followers
- Alexis Corbière : 89.6K followers


### Gauche Démocrate et Républicaine

- André Chassaigne : 12.4K


### Rassemblement National

- Marine Le Pen, 2.4M followers


# Agreement in time and Visualization

**IN CONSTRUCTION**
