from typing import List, Pattern
import argparse
import os
import sys
import json
import re

import numpy as np
from tqdm import tqdm
from sentence_transformers import SentenceTransformer
from sentence_transformers.losses import SoftmaxLoss
from googletrans import Translator, LANGUAGES
import torch

from twitter import Tweet
from train_nli import labels


arg_parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
arg_parser.add_argument(
    "-if",
    "--input-file",
    type=str,
    default=None,
    help="Input file path. If not specified, read from stdin.",
)
arg_parser.add_argument(
    "-of",
    "--output-file",
    type=str,
    default=None,
    help="Output file path. If not specified, print to stdout.",
)
arg_parser.add_argument(
    "-il",
    "--input-language",
    type=str,
    default="en",
    help=f"Input Datas language. If other than 'en', the input will be translated from the specified language to english before computing agreement. Use 'auto' if you're unsure, otherwise one of the keys of {str(LANGUAGES)}",
)
arg_parser.add_argument(
    "-mip",
    "--model-input-path",
    type=str,
    default="./models/default",
    help="path to a model trained using train_nli.py",
)
arg_parser.add_argument(
    "-fp",
    "--filter-pattern",
    type=str,
    default="[Mm]asks?",
    help="Tweets filter regex (for performance reasons, regex is applied _before_ translation. Please specify regex using the source language. If you need another behaviour, please raise an issue.)",
)
arg_parser.add_argument(
    "-n", "--normalize", action="store_true", help="wether to normalize or not"
)
args = arg_parser.parse_args()
args.model_input_path = args.model_input_path.strip("/")

print(f"running with config : {json.dumps(vars(args), indent=4)}", file=sys.stderr)


if args.input_file is None:
    raw_datas = sys.stdin.read()
elif os.path.exists(args.input_file):
    with open(args.input_file) as f:
        raw_datas = f.read()
else:
    print(f"input file {args.input_file} does not exist. exitting...", file=sys.stderr)
    exit()


print("* loading model...", file=sys.stderr)

sentence_bert_model = SentenceTransformer(f"{args.model_input_path}/embedding")

with open(f"{args.model_input_path}/classifier/config.json") as f:
    classifier_config = json.loads(f.read())
concat_vectors_nb = 0
if classifier_config["concatenation_sent_rep"]:
    concat_vectors_nb += 2
if classifier_config["concatenation_sent_difference"]:
    concat_vectors_nb += 1
if classifier_config["concatenation_sent_multiplication"]:
    concat_vectors_nb += 1
classifier = torch.nn.Linear(
    classifier_config["embedding_dimension"] * concat_vectors_nb, len(labels.keys())
)
classifier.load_state_dict(
    torch.load(
        f"{args.model_input_path}/classifier/classifier.pth",
        map_location=torch.device("cuda" if torch.cuda.is_available() else "cpu"),
    ),
    strict=False,
)

print("* loading tweets...", file=sys.stderr)
tweet_datas = [Tweet(**t) for t in json.loads(raw_datas)]
print(f"loaded {len(tweet_datas)} tweets", file=sys.stderr)


def search_if_keyword(tweet: Tweet, filter_pattern: Pattern):
    if re.search(filter_pattern, tweet.text):
        return True
    return False


print("* filtering datas...", file=sys.stderr)
filter_pattern = re.compile(args.filter_pattern)
tweet_datas: List[Tweet] = list(
    filter(lambda e: search_if_keyword(e, filter_pattern), tweet_datas)
)
print(f"{len(tweet_datas)} remaining tweets")

if args.input_language != "en":
    print(f"* translating tweets to english (source language : {args.input_language})")
    translator = Translator()
    for tweet in tqdm(tweet_datas):
        tweet.text = translator.translate(tweet.text, src=args.input_language).text

politicians = set([tweet.author for tweet in tweet_datas])
prev_tweets_repr = {politician: None for politician in politicians}
interactions = []
interactions_count = {politician: 0 for politician in politicians}


print(f"* performing analysis...", file=sys.stderr)

for tweet in tqdm(tweet_datas, file=sys.stderr):

    tweet_repr = torch.from_numpy(
        sentence_bert_model.encode([tweet.text])[0]
    ).unsqueeze(0)

    for other_politician in politicians - {tweet.author}:

        if prev_tweets_repr[other_politician] is None:
            continue

        other_tweet_repr = prev_tweets_repr[other_politician]

        repr_concat = []
        if classifier_config["concatenation_sent_rep"]:
            repr_concat.append(tweet_repr)
            repr_concat.append(other_tweet_repr)
        if classifier_config["concatenation_sent_difference"]:
            repr_concat.append(torch.abs(tweet_repr - other_tweet_repr))
        if classifier_config["concatenation_sent_multiplication"]:
            repr_concat.append(tweet_repr, other_tweet_repr)
        out = torch.softmax(classifier(torch.cat(repr_concat, 1)), 1)

        result = torch.max(out, 1).indices[0].item()

        if result == labels["neutral"]:
            continue

        interactions.append(
            {
                "activeCharacter": tweet.author,
                "targetCharacters": [
                    {
                        "name": other_politician,
                        "influence": out[0][result].item()
                        if result == labels["entailment"]
                        else -out[0][result].item(),
                    }
                ],
            }
        )
        interactions_count[tweet.author] += 1
        interactions_count[other_politician] += 1
    prev_tweets_repr[tweet.author] = tweet_repr

if args.normalize and len(interactions) > 0:
    mean_similarity = sum(
        interaction["targetCharacters"][0]["influence"] for interaction in interactions
    ) / len(interactions)
    for interaction in interactions:
        interaction["targetCharacters"][0]["influence"] -= mean_similarity


to_write = json.dumps(
    {
        "characters": [
            {"name": politician, "interactionsCount": interactions_count[politician]}
            for politician in politicians
        ],
        "interactions": interactions,
    },
    indent=4,
)
if args.output_file:
    with open(args.output_file, "w") as f:
        f.write(to_write)
else:
    print(json.dumps(to_write, indent=4))
