from typing import List, Optional
from datetime import date
from urllib import parse as urlparse
import concurrent.futures
import sys
import re
import time

import requests
from bs4 import BeautifulSoup
from tqdm import tqdm


class Tweet:
    def __init__(self, text: str, date: str, author: str):
        self.text: str = text
        self.date: str = date
        self.author: str = author

    def __str__(self) -> str:
        return f"[{self.author}] : {self.text}"


MONTHS = {
    "Jan": 1,
    "Feb": 2,
    "Mar": 3,
    "Apr": 4,
    "May": 5,
    "Jun": 6,
    "Jul": 7,
    "Aug": 8,
    "Sep": 9,
    "Oct": 10,
    "Nov": 11,
    "Dec": 12,
}
USER_AGENT = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)"
MAX_RETRY_COUNT = 10
MAX_WORKER = 50


def _date_to_iso(input_date: str) -> str:
    """ parse HH:MM [AM|PM] - D?D M+ YYYY to YYYY-MM-DD
    :throws: Exception if match was not succesfull
    :return: YYYY-MM-DD string if the date was correctly parsed
    """
    match = re.search(
        r"tweeted at [0-9]+:[0-9]+ [A|P]M - ([0-9]+) ([A-Za-z]+) ([0-9]+)", input_date
    )
    day = match.group(1)
    month = match.group(2)
    year = match.group(3)

    if day is None or month is None or year is None:
        raise Exception(f"could not convert {input_date} to iso")

    if len(day) < 2:
        day = "0" + day

    month = str(MONTHS[month])
    if len(month) < 2:
        month = "0" + month

    return f"{year}-{month}-{day}"


def _request(url: str, session: requests.Session, verbose: bool) -> Optional[str]:

    retry_count = 0

    while True:

        if verbose:
            tqdm.write(f"GET {url}", file=sys.stderr)

        try:
            answer = session.get(url, headers={"User-Agent": USER_AGENT})
        except Exception as e:
            tqdm.write(str(e), file=sys.stderr)
            tqdm.write("error performing request. retrying...", file=sys.stderr)
            retry_count += 1
            if retry_count > MAX_RETRY_COUNT:
                tqdm.write("too many retries. aborting...", file=sys.stderr)
                return None
            time.sleep(1)
            continue

        if answer.status_code == 200:
            return answer.text
        elif answer.status_code == 429:
            tqdm.write("request denied with 429. retrying...", file=sys.stderr)
            retry_count += 1
            if retry_count > MAX_RETRY_COUNT:
                tqdm.write("too many retries. aborting...", file=sys.stderr)
                return None
            time.sleep(1)
        else:
            tqdm.write(f"request denied with {answer.status_code}", file=sys.stderr)
            return None


def load_tweet(
    author: str,
    tweet_url: str,
    limit_date: date,
    progress_bar: tqdm,
    session: requests.Session,
    verbose: bool,
) -> Optional[Tweet]:

    answer = _request(tweet_url, session, verbose)
    if answer is None:
        tqdm.write("error downloading tweet - skipping...", file=sys.stderr)
        return None

    tweet_soup = BeautifulSoup(answer, features="lxml")

    try:
        raw_tweet_date = (
            tweet_soup.find("div", "timeline").find("div", "action").find("a")["href"]
        )
    except Exception as e:
        tqdm.write(str(e), file=sys.stderr)
        tqdm.write("could not find tweet metadatas - skipping...", file=sys.stderr)
        return None
    tweet_date = date.fromisoformat(_date_to_iso(urlparse.unquote(raw_tweet_date)))
    progress_bar.set_postfix({"date": str(tweet_date)})

    try:
        tweet_text = (
            tweet_soup.find("tr", "tweet-container").find("div", "tweet-text").text
        )
    except Exception as e:
        tqdm.write(str(e), file=sys.stderr)
        tqdm.write("could not retrieve tweet text - skipping...", file=sys.stderr)
        return None

    return Tweet(tweet_text, str(tweet_date), author)


def load_tweets(
    author: str,
    account_url: str,
    limit_date: date,
    progress_bar: tqdm,
    session: Optional[requests.Session] = None,
    verbose: bool = False,
) -> List[Tweet]:

    ganged_author_profile_link_name = account_url.split("/")[-1]
    author_profile_link_name = re.sub(
        r"^([^?]*).*$", r"\1", ganged_author_profile_link_name
    )

    if session is None:
        session = requests.sessions.Session()

    answer = _request(account_url, session, verbose)
    if answer is None:
        tqdm.write("could not perform wall request - returning...")
        return []

    soup = BeautifulSoup(answer, features="lxml")
    tweet_tables = soup.find_all("table", "tweet")
    tweets_url = []
    for tweet_table in tweet_tables:
        try:
            ganged_profile_link_name = tweet_table.find("td", class_="user-info").find(
                "a"
            )["href"]
            profile_link_name = re.sub(r"\/(.*?)\?.*", r"\1", ganged_profile_link_name)
            if profile_link_name != author_profile_link_name:
                if verbose:
                    tqdm.write(
                        f"filtered tweet from {profile_link_name} on {author_profile_link_name} profile",
                        file=sys.stderr,
                    )
                continue
        except Exception as e:
            tqdm.write(str(e), file=sys.stderr)
            continue
        tweets_url.append(
            "https://twitter.com"
            + tweet_table.find("td", "meta-and-actions").find("a", "last")["href"]
        )

    tweets = []
    with concurrent.futures.ThreadPoolExecutor(max_workers=MAX_WORKER) as executor:
        tasks = [
            executor.submit(
                load_tweet,
                author,
                tweet_url,
                limit_date,
                progress_bar,
                session,
                verbose,
            )
            for tweet_url in tweets_url
        ]
        for task in concurrent.futures.as_completed(tasks):
            tweet = task.result()
            tweets.append(tweet)

    filtered_tweets = []
    should_return = None
    for tweet in filter(lambda e: not e is None, tweets):
        if date.fromisoformat(tweet.date) < limit_date:
            should_return = tweet.date
            continue
        filtered_tweets.append(tweet)
    if verbose:
        tqdm.write(
            f"aggregated {len(filtered_tweets)} tweets from {account_url}",
            file=sys.stderr,
        )
    if not should_return is None:
        if verbose:
            tqdm.write(
                f"tweet too old ({should_return}) - returning...", file=sys.stderr
            )
        return filtered_tweets

    older_tweets_button = soup.find("div", "w-button-more")
    if older_tweets_button is None:
        tqdm.write("no next button found - returning...", file=sys.stderr)
        return filtered_tweets

    new_url_suffix = older_tweets_button.find("a").get("href")
    new_url = "https://mobile.twitter.com" + new_url_suffix
    filtered_tweets += load_tweets(
        author, new_url, limit_date, progress_bar, session, verbose
    )

    return filtered_tweets
