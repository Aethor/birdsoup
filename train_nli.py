from typing import *
import functools
import math
import argparse
import json
import sys
import os
import zipfile

import requests
from tqdm import tqdm
from sentence_transformers import SentenceTransformer, SentencesDataset
from sentence_transformers import models
from sentence_transformers.evaluation import LabelAccuracyEvaluator
from sentence_transformers.losses import SoftmaxLoss
from sentence_transformers.readers import InputExample
import torch


labels: Mapping[str, int] = {"contradiction": 0, "entailment": 1, "neutral": 2}


def dataset_or_die(
    url: str, datas_dir: str, dataset_zipname: str, dataset_dirname: str
):

    datas_zippath = f"{datas_dir}/{dataset_zipname}"
    datas_dataset_path = f"{datas_dir}/{dataset_dirname}"

    if not os.path.isdir(datas_dataset_path):

        print(f"downloading {dataset_dirname} dataset at {url}...", file=sys.stderr)
        answer = requests.get(url, stream=True)
        if answer.status_code != 200:
            print(
                f"error : could not download {dataset_dirname} dataset. exitting...",
                file=sys.stderr,
            )
            exit()
        block_size = 1024
        total_size = int(int(answer.headers.get("content-length", 0)) / block_size)

        with open(datas_zippath, "wb") as f:
            for data in tqdm(
                answer.iter_content(block_size), total=total_size, unit="KiB"
            ):
                f.write(data)

        with zipfile.ZipFile(datas_zippath, "r") as f:
            f.extractall(datas_dir)


def load_examples(
    filepath: str, example_filter: Optional[Callable[[dict], bool]] = None,
) -> List[InputExample]:

    line_nb = 0
    with open(filepath, "r") as f:
        for line in f:
            line_nb += 1

    examples = []
    guid = 0
    with open(filepath, "r") as f:

        for line in tqdm(f, total=line_nb):

            example = json.loads(line)

            if not example["gold_label"] in labels:
                continue

            if not example_filter is None:
                if example_filter(example):
                    continue

            examples.append(
                InputExample(
                    guid=guid,
                    texts=[example["sentence1"], example["sentence2"]],
                    label=labels[example["gold_label"]],
                )
            )
            guid += 1

    return examples


if __name__ == "__main__":

    arg_parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    arg_parser.add_argument(
        "-dd", "--datas-dir", default="./datas", type=str, help="datas directory"
    )
    arg_parser.add_argument(
        "-btm",
        "--base-transformer-model",
        default="roberta-base",
        type=str,
        help="Base transformer model",
    )
    arg_parser.add_argument(
        "-bz", "--batch-size", default=16, type=int, help="batch size"
    )
    arg_parser.add_argument(
        "-en", "--epochs-number", default=1, type=int, help="epochs number"
    )
    arg_parser.add_argument(
        "-mop",
        "--model-output-path",
        default="./models/default",
        type=str,
        help="model output path",
    )
    args = arg_parser.parse_args()
    args.datas_dir = args.datas_dir.strip("/")
    args.model_output_path = args.model_output_path.strip("/")

    print(f"running with config : {json.dumps(vars(args), indent=4)}", file=sys.stderr)

    if not os.path.isdir(args.datas_dir):
        print(
            f"error : invalid datas directory ({args.datas_dir}). exitting...",
            file=sys.stderr,
        )
        exit()

    print("* loading model...", file=sys.stderr)
    word_embedding_model = models.Transformer(args.base_transformer_model)
    pooling_model = models.Pooling(
        word_embedding_model.get_word_embedding_dimension(),
        pooling_mode_mean_tokens=True,
        pooling_mode_cls_token=False,
        pooling_mode_max_tokens=False,
    )
    sentence_bert_model = SentenceTransformer(
        modules=[word_embedding_model, pooling_model]
    )
    classifier = SoftmaxLoss(
        sentence_bert_model,
        sentence_bert_model.get_sentence_embedding_dimension(),
        len(list(labels.keys())),
    )

    print("* loading datas...", file=sys.stderr)

    multinli_zipname = "multinli_1.0.zip"
    multinli_dirname = "multinli_1.0"
    dataset_or_die(
        "https://cims.nyu.edu/~sbowman/multinli/multinli_1.0.zip",
        args.datas_dir,
        multinli_zipname,
        multinli_dirname,
    )

    print("loading train datas...", file=sys.stderr)
    raw_train_datas = load_examples(
        f"{args.datas_dir}/{multinli_dirname}/multinli_1.0_train.jsonl"
    )
    print("creating train dataset...", file=sys.stderr)
    train_datas = SentencesDataset(raw_train_datas, model=sentence_bert_model)
    train_dataloader = torch.utils.data.DataLoader(
        train_datas, shuffle=False, batch_size=args.batch_size
    )

    print("loading test datas...", file=sys.stderr)
    raw_test_datas = load_examples(
        f"{args.datas_dir}/{multinli_dirname}/multinli_1.0_dev_matched.jsonl"
    )
    print("creating test dataset...", file=sys.stderr)
    test_datas = SentencesDataset(raw_test_datas, model=sentence_bert_model)
    test_dataloader = torch.utils.data.DataLoader(
        test_datas, shuffle=False, batch_size=args.batch_size
    )

    print("* loading evaluators...")
    evaluator = LabelAccuracyEvaluator(test_dataloader, softmax_model=classifier)

    print("* starting trainig run...", file=sys.stderr)

    sentence_bert_model.fit(
        train_objectives=[(train_dataloader, classifier)],
        epochs=args.epochs_number,
        evaluator=evaluator,
        warmup_steps=math.ceil(
            len(train_dataloader) * args.epochs_number / args.batch_size * 0.1
        ),
        output_path=f"{args.model_output_path}/embedding",
    )

    os.mkdir(f"{args.model_output_path}/classifier")
    torch.save(
        classifier.classifier.state_dict(),
        f"{args.model_output_path}/classifier/classifier.pth",
    )
    with open(f"{args.model_output_path}/classifier/config.json", "w") as f:
        f.write(
            json.dumps(
                {
                    "embedding_dimension": sentence_bert_model.get_sentence_embedding_dimension(),
                    "concatenation_sent_rep": classifier.concatenation_sent_rep,
                    "concatenation_sent_difference": classifier.concatenation_sent_difference,
                    "concatenation_sent_multiplication": classifier.concatenation_sent_multiplication,
                },
                indent=4,
            )
        )

    print(f"accuracy : {evaluator(sentence_bert_model)}", file=sys.stderr)
