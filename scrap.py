import json
import sys
import argparse
from datetime import date

from tqdm import tqdm

from twitter import load_tweets


arg_parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
arg_parser.add_argument(
    "-v",
    "--verbose",
    action="store_true",
    help="If specified, the script will output more details",
)
arg_parser.add_argument(
    "-ld",
    "--limit-date",
    type=str,
    default="2019-12-01",
    help="Limit date for scrapping as an ISO timestamp (YYYY-MM-DD)",
)
arg_parser.add_argument(
    "-if",
    "--input-file",
    type=str,
    help="Input file with political figures",
    default="figures.json",
)
arg_parser.add_argument(
    "-of",
    "--output-file",
    type=str,
    help="Output file. If not specified, print to standard output",
    default=None,
)
args = arg_parser.parse_args()


limit_date = date.fromisoformat(args.limit_date)
datas = json.loads(open(args.input_file).read())

tweets = {
    member_name: {"tweets": [], "party": party_name}
    for party_name, party_attrs in datas.items()
    for member_name in party_attrs["members"].keys()
}

tweets = []
party_nb = len(list(datas.keys()))

politicians = [
    (member, party_name)
    for party_name, party_attrs in datas.items()
    for member in party_attrs["members"]
]
progress_bar = tqdm(politicians)


for politician, party_name in progress_bar:

    politician_attrs = datas[party_name]["members"][politician]

    progress_bar.set_description("{:15.15s}".format(politician))
    progress_bar.set_postfix({"date": str(date.today())})

    tweets += [
        vars(tweet)
        for tweet in load_tweets(
            politician,
            politician_attrs["twitter"],
            limit_date,
            progress_bar,
            verbose=args.verbose,
        )
    ]


if args.verbose:
    print("sorting by date...", file=sys.stderr)
tweets.sort(key=lambda t: date.fromisoformat(t["date"]))


if args.output_file is None:
    print(json.dumps(tweets, indent=4, ensure_ascii=False))
else:
    with open(args.output_file, "w", encoding="utf-8") as f:
        json.dump(tweets, f, indent=4, ensure_ascii=False)
